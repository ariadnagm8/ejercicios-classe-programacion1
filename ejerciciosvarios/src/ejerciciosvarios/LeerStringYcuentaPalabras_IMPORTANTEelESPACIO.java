package ejerciciosvarios;

import java.util.Scanner;

public class LeerStringYcuentaPalabras_IMPORTANTEelESPACIO {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Algorisme que llegeix una seqüència de lletres (que formen una frase) fins
		// arribar al punt. La sortida ens diu quantes paraules conté la frase introduïda.
		
		Scanner sc = new Scanner(System.in);

		String frase;
		frase = sc.nextLine(); //escribo la frase

		int longitud = frase.length(); //frase.length me dice la longitud de frase, que era mi escaner
		int contadorParaContar = 0;

		for (int i = 0; i <longitud; i++) { 
			if (frase.charAt(i) == ' ') {
				contadorParaContar++;
			}
			
		}
		System.out.println(contadorParaContar + 1);
	} 
}