package ejerciciosvarios;

import java.util.Scanner;

public class String_como_funciona {
	public static void main(String[] args) {


	Scanner sc = new Scanner(System.in);
	int a = 4;
	
	String st = "patata";
	////Metodes sobre les strings
	
	
	///utils
	String mayus = st.toUpperCase();
	System.out.println(mayus); // imprime el string st=patata escrito arriba en mayuscula
	System.out.println(mayus.toLowerCase()); // imprime el string st=patata escrito arriba en minuscula
	
	
	///recorrer String
	
	System.out.println(st.length()); //lee el string y te dice el numero de letras, de patata son 6
	
	System.out.println(st.charAt(0)); // lee el string y t dice qual es el primer caracter, en este caso "p"
	
	
	
	//contar A
	int longitud = st.length();  //longitud de la string (quantes lletres te)
	int contadorParaContar = 0;  
	for(int i = 0; i < longitud; i++) {   //va desde 0 fins a la longitud de la string (recorre la String)
		char caracterActual = st.charAt(i);  // caracter actual en i
		if(caracterActual=='a') {   ///
			contadorParaContar++; 
		}
	}
	System.out.println(contadorParaContar);
	
	
	
	///substring
	String subs = st.substring(3, 6);
	System.out.println(subs);
	
	//Split
	String frase = "hola me gustan las patatas y los gatetes y git no";
	String[] array = frase.split(" ");
	System.out.println(array.length);
	
	
	///conversion
	
	String coord = "G8";
	
	int fila;
	int columna;
	
	char primeraCoord = coord.charAt(0);
	String segundaCoord = coord.substring(1, 2);
	
	System.out.println(segundaCoord);
	//la primera se haria con un switch
	
	int segcoord = Integer.parseInt(segundaCoord);
	
	segcoord++;
	
	System.out.println(segcoord);
	}
}