package ejerciciosvarios;

import java.util.Scanner;

public class FlagquecCuentaLOSnumerosDIEZqueHAY {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// algoritmo que lee hasta que encuentra un -1 y si uno de esos num es 10, nos
		// dice cuantos hay

		// se introduce un contador;

		Scanner sc = new Scanner(System.in);
		int nota;
		nota = sc.nextInt();

		int contador = 0;

		boolean flag = false;
		// fem codi

		while (nota != -1) {
			if (nota == 10) {
				flag = true;
				contador++; // cuando el flag es true entonces sí añado el contador
			}
			// dins de bucle necesito el valor de nota, pq sino sempre ser elvalor inicial d
			// entrada al bucle, si d'inici nota = 9 per exemple, estaria en bucle
			// infinitament
			nota = sc.nextInt();
		} // cierro bucle

		System.out.println("en tus números tienes " + contador + " veces el num 10"); 
		// sale false si ningun numero es 10
		// cuando llego a -1 ha salido algun 10
																					
		// sale true cuando al escribir -1 algun numero de esos ha sido el 10

	}

}
